#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, cycle_length, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "100 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 1000)

    def test_read3(self):
        s = "25 345\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  25)
        self.assertEqual(j, 345)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(200, 175)
        self.assertEqual(v, 120)

    def test_eval_case1(self):
        v = collatz_eval(2001, 3000)
        self.assertEqual(v, 217)

    def test_eval_case2(self):
        v = collatz_eval(3500, 5000)
        self.assertEqual(v, 238)

    def test_eval_case3(self):
        v = collatz_eval(3001, 5500)
        self.assertEqual(v, 238)

    def test_eval_case4(self):
        v = collatz_eval(3500, 6500)
        self.assertEqual(v, 262)

    def test_eval_case5(self):
        v = collatz_eval(3500, 4600)
        self.assertEqual(v, 238)

    def test_eval_6(self):
        v = collatz_eval(18140, 13517)
        self.assertEqual(v, 279)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 3, 40, 112)
        self.assertEqual(w.getvalue(), "3 40 112\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 2000, 45678, 324)
        self.assertEqual(w.getvalue(), "2000 45678 324\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("2 20\n300 400\n501 510\n600 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 20 21\n300 400 144\n501 510 111\n600 1000 179\n")

    def test_solve3(self):
        r = StringIO("6 70\n80 90\n3 125\n400 450\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "6 70 113\n80 90 111\n3 125 119\n400 450 134\n")

    def test_solve5(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
